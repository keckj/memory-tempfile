import os
import sys
import platform
from collections import OrderedDict
import tempfile
from backports import tempfile as tempfile_backport

MEM_BASED_FS = ['tmpfs', 'ramfs']
SUITABLE_PATHS = ['/run/user/{uid}', '/run/shm', '/dev/shm', '/tmp']

class MemoryTempfile:
    def __init__(self, preferred_paths=None, remove_paths=None,
                 additional_paths=None, filesystem_types=None, fallback=None):
        self.os_tempdir = tempfile.gettempdir()
        
        if isinstance(fallback, bool):
            fallback = self.os_tempdir if fallback else None

        if platform.platform().startswith('Linux'):
            self.filesystem_types = filesystem_types if filesystem_types is not None else MEM_BASED_FS
            
            preferred_paths = [] if preferred_paths is None else preferred_paths
            remove_paths = [] if remove_paths is None else remove_paths
            additional_paths = [] if additional_paths is None else additional_paths

            suitable_paths = list(preferred_paths) + list(SUITABLE_PATHS) + list(additional_paths) + [self.os_tempdir]
            self.suitable_paths = list(filter(lambda path: path not in remove_paths, suitable_paths))
    
            uid = os.geteuid()
            
            with open('/proc/self/mountinfo', 'r') as file:
                mnt_info = {i[2]: i for i in [line.split() for line in file]}
            
            usable_paths = []
            for path in self.suitable_paths:
                path = path.replace('{uid}', str(uid))
                
                # We may have repeated
                if path in usable_paths:
                    continue
                try:
                    dev = os.stat(path).st_dev
                    major, minor = os.major(dev), os.minor(dev)
                    mp = mnt_info.get('{}:{}'.format(major, minor))
                    if mp and mp[8] in self.filesystem_types:
                        usable_paths.append(path)
                except EnvironmentError:
                    pass
            
            if len(usable_paths) > 0:
                self.tempdir = usable_paths[0]
            else:
                if fallback:
                    self.tempdir = fallback
                else:
                    raise RuntimeError('No memory temporary dir found and fallback is disabled.')
            self.usable_paths = usable_paths
        else:
            raise NotImplementedError('Unsupported platform {}'.format(platform.platform()))

    def found_mem_tempdir(self):
        return len(self.usable_paths) > 0
    
    def using_mem_tempdir(self):
        return self.tempdir in self.usable_paths
    
    def get_usable_mem_tempdir_paths(self):
        return self.usable_paths[:]
    
    def gettempdir(self):
        return self.tempdir

    def gettempdirb(self):
        return self.tempdir.encode(sys.getfilesystemencoding(), 'surrogateescape')

    def mkdtemp(self, suffix='', prefix='tmp', dir=None):
        return tempfile.mkdtemp(suffix=suffix, prefix=prefix, dir=self.tempdir if not dir else dir)

    def mkstemp(self, suffix='', prefix='tmp', dir=None, text=False):
        return tempfile.mkstemp(suffix=suffix, prefix=prefix, dir=self.tempdir if not dir else dir, text=text)

    def TemporaryDirectory(self, suffix='', prefix='tmp', dir=None):
        return tempfile_backport.TemporaryDirectory(suffix=suffix, prefix=prefix, dir=self.tempdir if not dir else dir)

    def SpooledTemporaryFile(self, max_size=0, mode='w+b', suffix='', prefix='tmp', dir=None):
        return tempfile.SpooledTemporaryFile(max_size=max_size, mode=mode, encoding=encoding,
                                              suffix=suffix, prefix=prefix,
                                             dir=self.tempdir if not dir else dir)

    def NamedTemporaryFile(self, mode='w+b', suffix='', prefix='tmp', dir=None, delete=True):
        return tempfile.NamedTemporaryFile(mode=mode, 
                                           suffix=suffix, prefix=prefix, dir=self.tempdir if not dir else dir,
                                           delete=delete)

    def TemporaryFile(self, mode='w+b', suffix='', prefix='tmp', dir=None):
        return tempfile.TemporaryFile(mode=mode, 
                                      suffix=suffix, prefix=prefix, dir=self.tempdir if not dir else dir)

    def gettempprefix(self):
        return tempfile.gettempdir()

    def gettempprefixb(self):
        return tempfile.gettempprefixb()
